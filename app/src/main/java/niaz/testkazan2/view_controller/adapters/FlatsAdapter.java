package niaz.testkazan2.view_controller.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import java.util.List;
import niaz.testkazan2.R;
import niaz.testkazan2.view_controller.activities.FlatsActivity;
import niaz.testkazan2.view_controller.activities.MainActivity;
import niaz.testkazan2.model.Flat;

/**
 * Recycler view adapter
 */
public class FlatsAdapter extends RecyclerView.Adapter<FlatsAdapter.ViewHolder>// implements DataRequest
{
    private Context context;
    private List<Flat> flats;

    /**
     * Constructor
     */
    public FlatsAdapter(FlatsActivity activity, List<Flat> flats){
        context = activity.getApplicationContext();
        this.flats = flats;
    }

    /**
     * View Holder
     */
    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public EditText editTextDescription;
        public Button buttonDelete;

        public ViewHolder(View v){
            super(v);
            editTextDescription = (EditText) v.findViewById(R.id.flat_description);
            buttonDelete = (Button) v.findViewById(R.id.flat_delete);
        }
    }

    /**
     * On create holder
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public FlatsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        // Create a new View
        View v = LayoutInflater.from(context).inflate(R.layout.flat_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    /**
     * Bind view holder
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position){
        holder.editTextDescription.setText(flats.get(holder.getAdapterPosition()).getFlatDescription());
        holder.editTextDescription.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d(MainActivity.TAG, "changed pos=" + position + " str=" + s.toString());
                flats.get(position).setFlatDescription(s.toString());
            }
        });

        holder.buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(MainActivity.TAG, "delete pos=" + position);
            }
        });

    }

    /**
     * Get count
     * @return
     */
    @Override
    public int getItemCount(){
        return flats.size();
    }

    /**
     * Get flats
     * @return
     */
    public List<Flat> getFlats(){
        return flats;
    }

}