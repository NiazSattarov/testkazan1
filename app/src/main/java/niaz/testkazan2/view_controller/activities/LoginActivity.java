package niaz.testkazan2.view_controller.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import niaz.testkazan2.R;
import niaz.testkazan2.model.MyDatabase;

/**
 * Created by NS on 01.06.2017.
 */

public class LoginActivity extends AppCompatActivity {
    private final String MY_SHARED_PREFERENCES = "myPrefs";
    private final String LOGIN_ATTEMPTS = "attempts";
    private final int MAX_ATTEMPTS = 3;
    private final int TIME_DISABLE = 60000;
    private final int TIME_TICK = 500;

    private MyDatabase myDatabase;
    private ProgressBar progressBar;
    private EditText editTextLogin;
    private EditText editTextPassword;
    private TextView textViewInfo;
    private boolean disableButton = false;
    private CountDownTimer countDownTimer;
    private long timeRemaining;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        initViews();

        myDatabase = new MyDatabase(getApplicationContext());
    }

    private void initViews(){
        progressBar = (ProgressBar)findViewById(R.id.login_progress_bar);
        editTextLogin = (EditText)findViewById(R.id.edit_login);
        editTextPassword = (EditText)findViewById(R.id.edit_password);
        textViewInfo = (TextView)findViewById(R.id.login_info);

        findViewById(R.id.button_sign_in).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (disableButton){
                    return;
                }
                try {
                    myDatabase.createOrOpen();
                    if (!myDatabase.isOpen()){
                        showDatabaseError();
                        return;
                    }

                    checkLoginAsync(editTextLogin.getText().toString().trim(), editTextPassword.getText().toString().trim());
                }
                catch (IOException e){
                    showDatabaseError();
                }
            }
        });

    }

    private void showDatabaseError(){
        Log.e(MainActivity.TAG, getResources().getString(R.string.error_database));
        Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_database), Toast.LENGTH_LONG).show();
    }

    private void showError(String message){
        Log.e(MainActivity.TAG, message);
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    private boolean checkLogin(String login, String password){
        try {
            Thread.sleep(3000);
        }
        catch (InterruptedException e){
            Log.e(MainActivity.TAG, "Error to do delay");
        }

        try {
            return myDatabase.checkLoginAndPassword(editTextLogin.getText().toString().trim(), editTextPassword.getText().toString().trim());
        }
        catch (IOException e){
            return false;
        }

    }

    private void checkLoginAsync(String login, String password){
        progressBar.setVisibility(View.VISIBLE);
        Observable.fromCallable(() -> {
            return checkLogin(login, password);
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        result -> {
                            progressBar.setVisibility(View.INVISIBLE);
                            Log.d(MainActivity.TAG, "result=" + result);
                            if (!myDatabase.exists()){
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_database_not_created), Toast.LENGTH_LONG).show();
                                return;
                            }
                            if (result) {
                                Log.d(MainActivity.TAG, "Login OK");
                                clearAttemptsInPrefs();
                                Intent intent = new Intent(LoginActivity.this, HousesActivity.class);
                                startActivity(intent);
                            }
                            else {
                                saveAttemptsToPrefs();
                                showError(getResources().getString(R.string.error_login));
                            }

                        }
                );
    }

    private void saveAttemptsToPrefs(){
        SharedPreferences prefs = this.getSharedPreferences(
                MY_SHARED_PREFERENCES, Context.MODE_PRIVATE);

        int attempts = prefs.getInt(LOGIN_ATTEMPTS, 0);
        Log.d(MainActivity.TAG, "get attempts=" + attempts);

        attempts++;
        prefs.edit().putInt(LOGIN_ATTEMPTS, attempts).apply();

        if (attempts >= MAX_ATTEMPTS){
            runDisableTimer();
        }
    }

    private void clearAttemptsInPrefs(){
        SharedPreferences prefs = this.getSharedPreferences(
                MY_SHARED_PREFERENCES, Context.MODE_PRIVATE);

        prefs.edit().putInt(LOGIN_ATTEMPTS, 0).apply();
    }

    private void runDisableTimer(){
        textViewInfo.setVisibility(View.VISIBLE);
        disableButton = true;
        timeRemaining = TIME_DISABLE;
        countDownTimer = new CountDownTimer(TIME_DISABLE, TIME_TICK) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeRemaining -= TIME_TICK;
                textViewInfo.setText(getResources().getString(R.string.text_remaining_time) + " " +
                        String.valueOf(timeRemaining / 1000));
            }

            @Override
            public void onFinish() {
                textViewInfo.setVisibility(View.INVISIBLE);
                disableButton = false;
                clearAttemptsInPrefs();
            }
        }.start();
    }

    @Override
    protected void onStop(){
        super.onStop();
        if (countDownTimer != null){
            countDownTimer.cancel();
        }
    }

}