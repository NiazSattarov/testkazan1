package niaz.testkazan2.view_controller.activities;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import niaz.testkazan2.R;
import niaz.testkazan2.view_controller.adapters.HousesAdapter;
import niaz.testkazan2.model.House;
import niaz.testkazan2.model.MyDatabase;

/**
 * Created by NS on 01.06.2017.
 */
public class HousesActivity extends AppCompatActivity {
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private EditText editTextDescription;

    private HousesAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<House> houses;
    private MyDatabase myDatabase;
    private boolean dataChanged = false;
    private List<String> deletedHousesIds = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_houses);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        myDatabase = new MyDatabase(getApplicationContext());
        initViews();
    }

    /**
     * We have to clear update parameters after return to activity
     */
    @Override
    protected void onResume(){
        super.onResume();
        dataChanged = false;
        deletedHousesIds.clear();
    }

    /**
     * Init views
     */
    private void initViews(){
        progressBar = (ProgressBar) findViewById(R.id.houses_progress_bar);
        progressBar.setVisibility(View.VISIBLE);

        editTextDescription = (EditText) findViewById(R.id.house_description_new);

        findViewById(R.id.house_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String description = editTextDescription.getText().toString();
                if (description.trim().isEmpty()) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_data_not_defined), Toast.LENGTH_LONG).show();
                    return;
                }

                House house = new House(myDatabase.getHouseIdNext(), description);
                house.setNewItem(true);
                houses.add(house);
                dataChanged = true;
                initAdapter();
                editTextDescription.setText("");
            }
        });

        findViewById(R.id.button_houses_save_changes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (progressBar.getVisibility() == View.VISIBLE){
                    return;
                }
                if (dataChanged) {
                    progressBar.setVisibility(View.VISIBLE);
                    updateHousesRequest();
                }
                else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.message_not_changed), Toast.LENGTH_LONG).show();
                }
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.houses_recycler_view);
        getHousesRequest();
    }

    /**
     * Add houseId to delete list
     * @param deletedHouseId
     */
    public void addDeletedHouseId(String deletedHouseId){
        deletedHousesIds.add(deletedHouseId);
    }

    /**
     * Init adapter
     */
    public void initAdapter(){
        if (houses != null) {
            adapter = new HousesAdapter(this, houses);
            recyclerView.setAdapter(adapter);
            layoutManager = new GridLayoutManager(getApplicationContext(), 1);
            recyclerView.setLayoutManager(layoutManager);
        }
    }

    /**
     * Set parameter which says that data is changed
     */
    public void setDataChanged(){
        dataChanged = true;
    }

    /**
     * Getter for dataChanged
     * @return
     */
    public boolean isDataChanged(){
        return dataChanged;
    }

    /**
     * Get houses request
     */
    private void getHousesRequest(){
        progressBar.setVisibility(View.VISIBLE);
        Observable.fromCallable(() -> {
            return getHousesFromDatabase();
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        houses -> {
                            progressBar.setVisibility(View.INVISIBLE);
                            Log.d(MainActivity.TAG, "loaded houses=" + houses.size());
                            this.houses = houses;
                            if (houses == null){
                                Log.e(MainActivity.TAG, getResources().getString(R.string.error_database));
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_database), Toast.LENGTH_LONG).show();
                                return;
                            }
                            initAdapter();
                        }
                );
    }

    /**
     * Get houses
     * @return
     */
    private List<House> getHousesFromDatabase(){
        Log.d(MainActivity.TAG, "getHousesFromDatabase");
        try {
            Thread.sleep(3000);
        }
        catch (InterruptedException e){
            Log.e(MainActivity.TAG, "Error to do delay");
        }

        try {
            myDatabase.createOrOpen();
            List<House> houses = myDatabase.getHouses();
            return houses;
        }
        catch (IOException e){
            return null;
        }
    }

    /**
     * UpdateHouseRequest
     */
    private void updateHousesRequest(){
        progressBar.setVisibility(View.VISIBLE);
        Observable.fromCallable(() -> {
            return updateHousesInDatabase();
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        houses -> {
                            progressBar.setVisibility(View.INVISIBLE);
                            Log.d(MainActivity.TAG, "returned houses=" + houses.size());
                            this.houses = houses;
                            dataChanged = false;
                            if (houses == null){
                                Log.e(MainActivity.TAG, getResources().getString(R.string.error_database));
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_database), Toast.LENGTH_LONG).show();
                                return;
                            }
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.message_update), Toast.LENGTH_LONG).show();
                            initAdapter();
                        }
                );
    }

    /**
     * Update houses
     * @return
     */
    private List<House> updateHousesInDatabase(){
        try {
            Thread.sleep(3000);
        }
        catch (InterruptedException e){
            Log.e(MainActivity.TAG, "Error to do delay");
        }


        try {
            List<House> result = myDatabase.updateHouses(houses, deletedHousesIds);
            return result;
        }
        catch (IOException e){
            return null;
        }
    }

}