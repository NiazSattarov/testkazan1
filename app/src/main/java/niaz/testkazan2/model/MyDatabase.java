package niaz.testkazan2.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import niaz.testkazan2.view_controller.activities.MainActivity;

/**
 * Database support
 * Created by NS on 01.06.2017.
 */
public class MyDatabase {
    private final String DATABASE_NAME = "myDatabase.db";
    private final String DATABASE_FOLDER = "/data";

    private final String TABLE_HOUSES = "Houses";
    private final String HOUSE_ID = "houseId";
    private final String HOUSE_DESCRIPTION = "houseDescription";

    private final String TABLE_FLATS = "Flats";
    private final String FLAT_ID = "flatHouseId";
    private final String FLAT_DESCRIPTION = "flatDescription";

    private final String TABLE_USERS = "Users";
    private final String USER_NAME = "Login";
    private final String USER_PASSWORD = "Password";

    private String houseIdLast = "0";
    private String flatIdLast = "0";

    SQLiteDatabase db;
    Context context;
    String databasePath;

    /**
     * Constructor
     * @param context
     */
    public MyDatabase(Context context) {
        this.context = context;
        databasePath = Environment.getExternalStorageDirectory().getAbsolutePath() + DATABASE_FOLDER;
        File folder = new File(databasePath);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        databasePath += "/" + DATABASE_NAME;
    }

    public String getHouseIdLast(){
        return houseIdLast;
    }

    /**
     * Get next house id
     * @return
     */
    public String getHouseIdNext(){
        try {
            int num = Integer.parseInt(houseIdLast);
            num++;
            String houseIdNext = String.valueOf(num);
            return houseIdNext;
        }
        catch (NumberFormatException e){
            Log.e(MainActivity.TAG, "Invalid houseId database=" + houseIdLast);
            return "";
        }
    }

    public String getFlatIdLast(){
        return flatIdLast;
    }

    /**
     * Delete database file
     * @throws IOException
     * @throws android.database.sqlite.SQLiteException
     */
    public void delete() throws IOException,
            android.database.sqlite.SQLiteException {
        context.deleteDatabase(databasePath);
    }

    /**
     * Create or open databse
     * @throws IOException
     * @throws android.database.sqlite.SQLiteException
     */
    public void createOrOpen() throws IOException,
            android.database.sqlite.SQLiteException {
        db = context.openOrCreateDatabase(databasePath, Context.MODE_PRIVATE, null);
    }

    /**
     * Close database
     * @throws IOException
     * @throws android.database.sqlite.SQLiteException
     */
    public void close() throws IOException,
            android.database.sqlite.SQLiteException {
        db.close();
    }

    /**
     * Is databse opened
     * @return
     */
    public boolean isOpen() {
        return db.isOpen();
    }

    /**
     * If file exists?
     * @return
     */
    public boolean exists(){
        File file = new File(databasePath);
        return file.exists();
    }

    /**
     * Create users
     * @throws IOException
     * @throws android.database.sqlite.SQLiteException
     */
    public void createUsers() throws IOException,
            android.database.sqlite.SQLiteException{
        db.execSQL("CREATE TABLE IF NOT EXISTS " +
                TABLE_USERS + " (" +
                USER_NAME + " VARCHAR," +
                USER_PASSWORD + " VARCHAR);");

        addRecord(TABLE_USERS, "user1", "111");
        addRecord(TABLE_USERS, "user2", "111");
        addRecord(TABLE_USERS, "user3", "111");
    }

    /**
     * Create houses
     * @throws IOException
     * @throws android.database.sqlite.SQLiteException
     */
    public void createHouses() throws IOException,
            android.database.sqlite.SQLiteException{
        db.execSQL("CREATE TABLE IF NOT EXISTS " +
                TABLE_HOUSES + " (" +
                HOUSE_ID + " VARCHAR," +
                HOUSE_DESCRIPTION + " VARCHAR);");

        addRecord(TABLE_HOUSES, "1", "Dubravnay, 5");
        addRecord(TABLE_HOUSES, "2", "Tolstogo, 15");
        addRecord(TABLE_HOUSES, "3", "Pushkina, 21");
    }

    /**
     * Create flats
     * @throws IOException
     * @throws android.database.sqlite.SQLiteException
     */
    public void createFlats() throws IOException,
            android.database.sqlite.SQLiteException{
        db.execSQL("CREATE TABLE IF NOT EXISTS " +
                TABLE_FLATS + " (" +
                FLAT_ID + " VARCHAR," +
                HOUSE_ID + " VARCHAR," +
                FLAT_DESCRIPTION +" VARCHAR);");

        addRecord(TABLE_FLATS, "1", "1", "Number 1, 1 room");
        addRecord(TABLE_FLATS, "2", "1", "Number 2, 2 rooms");
        addRecord(TABLE_FLATS, "3", "2", "Number 1, 3 rooms");
        addRecord(TABLE_FLATS, "4", "2", "Number 2, 4 rooms");
    }

    /**
     * Add record
     * @param table
     * @param param1
     * @param param2
     */
    private void addRecord(String table, String param1, String param2) {
        db.execSQL("INSERT INTO " + table + " VALUES('" +
                param1 + "','" +
                param2 + "');");
    }

    /**
     * Add record
     * @param table
     * @param param1
     * @param param2
     * @param param3
     */
    private void addRecord(String table, String param1, String param2, String param3) {
        db.execSQL("INSERT INTO " + table + " VALUES('" +
                param1 + "','" +
                param2 + "','" +
                param3 + "');");
    }

    /**
     * Print items
     * @throws IOException
     * @throws android.database.sqlite.SQLiteException
     */
    public void printItemsTable() throws IOException,
            android.database.sqlite.SQLiteException {
        if (db == null) {
            throw new IOException("Database instance is null");
        }
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_HOUSES, null);
        int i = 0;
        if (cursor.moveToFirst()) {
            int nameIndex = cursor.getColumnIndex(HOUSE_ID);
            int descriptionIndex = cursor.getColumnIndex(HOUSE_DESCRIPTION);

            do {
                String name = cursor.getString(nameIndex);
                String description = cursor.getString(descriptionIndex);
                i++;
                Log.d(MainActivity.TAG, "i=" + i + " name=" + name + " description=" + description);

            } while (cursor.moveToNext());
        }
    }

    /**
     * print users
     * @throws IOException
     * @throws android.database.sqlite.SQLiteException
     */
    public void printUsersTable() throws IOException,
            android.database.sqlite.SQLiteException {
        Log.d(MainActivity.TAG, "printUsersTable");
        if (db == null) {
            throw new IOException("Database instance is null");
        }
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_USERS, null);
        if (cursor.moveToFirst()) {
            int nameIndex = cursor.getColumnIndex(USER_NAME);
            int passwordIndex = cursor.getColumnIndex(USER_PASSWORD);

            do {
                String name = cursor.getString(nameIndex);
                String password = cursor.getString(passwordIndex);
                Log.d(MainActivity.TAG, "user=" + name + " password=" + password);

            } while (cursor.moveToNext());
        }
    }

    /**
     * Check login-password
     * @param user
     * @param password
     * @return
     * @throws IOException
     * @throws android.database.sqlite.SQLiteException
     */
    public boolean checkLoginAndPassword(String user, String password) throws IOException,
            android.database.sqlite.SQLiteException {

        Log.d(MainActivity.TAG, "checkLogin=" + user + " password=" + password);

        if (db == null) {
            throw new IOException("Database instance is null");
        }

        String query = "SELECT * FROM " +
                TABLE_USERS + " WHERE " + USER_NAME + "='" + user + "'";
        Cursor cursor = db.rawQuery(query, null);

        if (cursor == null || cursor.getCount() == 0) {
            Log.d(MainActivity.TAG, "++user " + user + " NOT found");
            return false;
        } else {
            Log.d(MainActivity.TAG, "cursor != null");
        }
        cursor.moveToFirst();

        int userPasswordIndex = cursor.getColumnIndex(USER_PASSWORD);
        String userPassword = cursor.getString(userPasswordIndex);
        if (userPassword.equals(password)) {
            Log.d(MainActivity.TAG, "user " + user + " found");
            return true;
        }
        Log.d(MainActivity.TAG, "user " + user + " NOT found");
        return false;
    }

    /**
     * Get houses
     * @return
     * @throws IOException
     * @throws android.database.sqlite.SQLiteException
     */
    public List<House> getHouses() throws IOException,
            android.database.sqlite.SQLiteException {
        List<House> houses = new ArrayList<>();

        if (db == null) {
            throw new IOException("Database instance is null");
        }
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_HOUSES, null);
        if (cursor.moveToFirst()) {
            int houseIdIndex = cursor.getColumnIndex(HOUSE_ID);
            int houseDescriptionIndex = cursor.getColumnIndex(HOUSE_DESCRIPTION);

            do {
                String houseId = cursor.getString(houseIdIndex);
                String houseDescription = cursor.getString(houseDescriptionIndex);
                Log.d(MainActivity.TAG, "get houseId=" + houseId + " houseDesc=" + houseDescription);
                House house = new House(houseId, houseDescription);
                houses.add(house);
                houseIdLast = houseId;

            } while (cursor.moveToNext());
        }
        return houses;
    }

    /**
     * Get flats
     * @param houseIdSelected
     * @return
     * @throws IOException
     * @throws android.database.sqlite.SQLiteException
     */
    public List<Flat> getFlats(String houseIdSelected) throws IOException,
            android.database.sqlite.SQLiteException {
        List<Flat> flats = new ArrayList<>();

        if (db == null) {
            throw new IOException("Database instance is null");
        }

        String query = "SELECT * FROM " +
                TABLE_FLATS + " WHERE " + HOUSE_ID + "='" + houseIdSelected + "'";
        Cursor cursor = db.rawQuery(query, null);

        if (cursor == null || cursor.getCount() == 0) {
            Log.d(MainActivity.TAG, "getFlats - houseId " + houseIdSelected + " NOT found");
            return null;
        } else {
            Log.d(MainActivity.TAG, "getFlats - cursor != null");
        }

        if (cursor.moveToFirst()) {
            int flatIdIndex = cursor.getColumnIndex(FLAT_ID);
            int flatDescriptionIndex = cursor.getColumnIndex(FLAT_DESCRIPTION);
            int houseIdIndex = cursor.getColumnIndex(HOUSE_ID);

            do {
                String flatId = cursor.getString(flatIdIndex);
                String flatDescription = cursor.getString(flatDescriptionIndex);
                String houseId = cursor.getString(houseIdIndex);

                Log.d(MainActivity.TAG, "flatId=" + flatId + " flatDesc=" + flatDescription + " houseId=" + houseId);
                Flat flat = new Flat(flatId, flatDescription, houseId);
                flats.add(flat);

            } while (cursor.moveToNext());
        }
        return flats;
    }

    /**
     * Update houses
     * @param houses
     * @param deletedHousesIds
     * @return
     * @throws IOException
     * @throws android.database.sqlite.SQLiteException
     */
    public List<House> updateHouses(List<House> houses, List<String> deletedHousesIds) throws IOException,
            android.database.sqlite.SQLiteException{
        for (House house : houses){
            if (house.isChanged()) {
                db.execSQL("UPDATE " + TABLE_HOUSES + " SET " +
                        HOUSE_DESCRIPTION + " = '" + house.getHouseDescription() + "'" +
                        " WHERE " + HOUSE_ID + " = '" + house.getHouseId() + "'");
                house.setChanged(false);
            }
            else if (house.isNewItem()){
                addRecord(TABLE_HOUSES, house.getHouseId(), house.getHouseDescription());
                house.setNewItem(false);
            }
        }

        Log.d(MainActivity.TAG, "update deletedHousesIds=" + deletedHousesIds.size());
        Log.d(MainActivity.TAG, "update deletedHousesIds=" + deletedHousesIds);
        for (String deletedHouseId : deletedHousesIds){
            db.execSQL("DELETE FROM " + TABLE_HOUSES+ " WHERE " + HOUSE_ID + "='" + deletedHouseId + "'");
        }

        return houses;
    }

}
