package niaz.testkazan2.model;

/**
 * Created by NS on 03.06.2017.
 */

public class House {
    private String houseId;
    private String houseDescription;
    private boolean changed = false;
    private boolean newItem = false;

    /**
     * Constructor
     * @param houseId
     * @param houseDescription
     */
    public House(String houseId, String houseDescription){
        this.houseId = houseId;
        this.houseDescription = houseDescription;
    }

    // Setters, getters
    public void setHouseId(String houseId){
        this.houseId = houseId;
    }

    public String getHouseId(){
        return houseId;
    }

    public void setHouseDescription(String houseDescription){
        this.houseDescription = houseDescription;
    }

    public String getHouseDescription(){
        return houseDescription;
    }

    public void setChanged(boolean changed){
        this.changed = changed;
    }

    public boolean isChanged(){
        return changed;
    }

    public void setNewItem(boolean newItem){
        this.newItem = newItem;
    }

    public boolean isNewItem(){
        return newItem;
    }

}
