package niaz.testkazan2.model;

/**
 * Created by NS on 03.06.2017.
 */

public class Flat {
    private String flatId;
    private String flatDescription;
    private String houseId;

    /**
     * Constructor
     * @param flatId
     * @param flatDescription
     * @param houseId
     */
    public Flat(String flatId, String flatDescription, String houseId){
        this.flatId = flatId;
        this.flatDescription = flatDescription;
        this.houseId = houseId;
    }

    // Setters, getters
    public void setFlatId(String flatId){
        this.flatId = flatId;
    }

    public String getFlatId(){
        return flatId;
    }

    public void setFlatDescription(String flatDescription){
        this.flatDescription = flatDescription;
    }

    public String getFlatDescription(){
        return flatDescription;
    }

    public void setHouseId(String houseId){
        this.houseId = houseId;
    }

    public String getHouseId(){
        return houseId;
    }


}
